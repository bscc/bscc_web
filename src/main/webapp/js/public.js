$(function() {
 /*   var activeNum = '';
    if(window.location.pathname.indexOf('index.html')>0){
        activeNum=0;
    }
    if(window.location.pathname.indexOf('method_list.html')>0){
        activeNum=1;
    }
    if(window.location.pathname.indexOf('productserver.html')>0){
        activeNum=2;
    }
    if(window.location.pathname.indexOf('case_list.html')>0){
        activeNum=3;
    }
    if(window.location.pathname.indexOf('newlist.html')>0){
        activeNum=4;
    }
    if(window.location.pathname.indexOf('about_us.html')>0){
        activeNum=5;
    }

    var navData = [
        {text: '首页',url: 'index.html'},
        {text: '解决方案',url: 'method_list.html'},
        {text: '产品服务',url: 'productserver.html'},
        {text: '客户案例',url: 'case_list.html'},
        {text: '新闻动态',url: 'newlist.html'},
        {text: '关于我们',url: 'about_us.html'},

    ]
    var li = '';
    navData.map(function(item,index){
        var list = ''
        if(activeNum==index){
            list = '<li class="navList"><a class="active" href=""'+item.url+'">'+item.text+'</a></li>';
        }else{
            list = '<li class="navList"><a href="'+item.url+'">'+item.text+'</a></li>';
        }
        li += list;
    })

    var headers = '<div id="nav">'
                     +'<nav>'
                         +'<div class="logoBox">'
                             +'<img src="images/logo.png" alt="">'
                         +'</div>'
                         +'<div class="navBox">'
                             +'<div class="sousuo" onclick="showNav()">'
                                +'<i class="fa fa-th-large"></i>'
                             +'</div>'
                             +'<ul>'
                                + li
                             +'</ul>'
                         +'</div>'
                     +'</nav>'
                 +'</div>';

    var footer = '<div id="footer">'
                     +'<div class="footerBox">'
                         +'<div class="footerboxLeft" data-scroll-reveal="enter left over 1s after 0.2s and move 20px">'
                             +'<div class="footerLeftLogo">'
                                +'<img src="images/logo2.png" alt="">'
                             +'</div>'
                             +'<div class="footerboxLeftText1">用科技传递诚信，用自信成就你我</div>'
                             +'<div class="footerboxLeftText2"><i class="fa fa-phone"></i>010-57225591</div>'
                             +'<div class="footerboxLeftText2"><i class="fa fa-envelope-o"></i>bscc@51un.cn</div>'
                             +'<div class="footerboxLeftText2"><i class="fa fa-map-marker"></i>北京市丰台区中福丽宫品牌基地4号楼138室</div>'
                         +'</div>'
                         +'<div class="footerboxRight">'
                             +'<div class="footerboxRightList" data-scroll-reveal="enter right over 0.5s after 0.2s and move 20px">'
                                +'<div class="footerboxRightListText">解决方案</div>'
                                 +'<ul>'
                                     +'<li>经济金融</li>'
                                     +'<li>电商</li>'
                                     +'<li>教育培训</li>'
                                     +'<li>企业服务</li>'
                                     +'<li>生活服务</li>'
                                     +'<li>更多行业解决方案</li>'
                                 +'</ul>'
                             +'</div>'
                             +'<div class="footerboxRightList" data-scroll-reveal="enter right over 0.5s after 0.4s and move 20px">'
                             +  '<div class="footerboxRightListText">产品服务</div>'
                                 +'<ul>'
                                     +'<li>只智能人工客服</li>'
                                     +'<li>人工在线客服</li>'
                                     +'<li>工单系统</li>'
                                     +'<li>本地部署方案</li>'
                                 +'</ul>'
                             +'</div>'
                             +'<div class="footerboxRightList" data-scroll-reveal="enter right over 0.5s after 0.6s and move 20px">'
                                 +'<div class="footerboxRightListText">关于我们</div>'
                                 +'<ul>'
                                     +'<li>公司介绍</li>'
                                     +'<li>联系我们</li>'
                                 +'</ul>'
                                +'<div class="footerboxRightListPic">'
                                    +'<img src="images/erweima.png" alt="">'
                                +'</div>'
                             +'</div>'
                         +'</div>'
                     +'</div>'
                 +'</div>'
                 +'<footer>'
                    +'© 北京博视创诚科技有限公司&nbsp;|&nbsp;版权所有&nbsp;&nbsp;京ICP备17013714号'
                 +'</footer>';

    //加载顶与底部
    $('.topBoxs').append(headers);
    // $('.topBoxs').load("public/header.html");
    $('.footerBoxs').append(footer);
    // $('.footerBoxs').load("public/footer.html");
*/

    var windowWidth = $(window).width();
    $(window).resize(function () {
        if(windowWidth!=$(window).width()){
            if(($(window).width()+17)>800){
                $('.navBox ul').width('auto');
            }else{
                $('.navBox ul').width('0px');
            }
        }

    });

});

function showNav(){
    if($('.navBox ul').width()==110){
        $('.navBox ul').width('0px');
    }else{
        $('.navBox ul').width('110px');
    }
}