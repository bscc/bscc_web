$(function(){

    //轮播图
    var mySwiper = new Swiper ('.swiper-container', {
        direction: 'horizontal',
        autoplay:5000,
        loop: true,

        // 如果需要前进后退按钮
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',

    })


    //关于我们的选项卡
    $('.aboutUsBoxRightTopList').on('click',function(){
        $(this).addClass('active').siblings().removeClass('active');
        $('.aboutUsBoxCenterBoxList').eq($(this).index()).show(200).siblings().hide();
    })

    //新闻动态的选项卡
    $('.newsFeedBoxTopTab').on('click',function(){
        $(this).addClass('active').siblings().removeClass('active');
        $('.newsFeedBoxCenterList').eq($(this).index()).show(200).siblings().hide();
    });

    $('.newListMore').on('click', function () {
        location.href='/news?channel='+$(this).attr('data-val');
    });

    $('.caseMore').on('click', function () {
        location.href='/caze';
    });

    $('.newsFeedBoxCenterListleft, .newList').on('click', function () {
        location.href='/newsDetail/'+$(this).attr('data-val');
    });


    $('.caseListDiv').on('click', function () {
        location.href='/cazeDetail/'+$(this).attr('data-val');
    });

    //页面滚动触发动画
    window.scrollReveal = new scrollReveal({ reset: true });




    /* config dom id (optional) + config particles params */
    /*particlesJS('particles-js', {
        particles: {
            color: '#000',
            shape: 'circle', // "circle", "edge" or "triangle"
            opacity: 1,
            size: 4,
            size_random: true,
            nb: 150,
            line_linked: {
                enable_auto: true,
                distance: 100,
                color: '#000',
                opacity: 1,
                width: 1,
                condensed_mode: {
                    enable: false,
                    rotateX: 600,
                    rotateY: 600
                }
            },
            anim: {
                enable: true,
                speed: 1
            }
        },
        interactivity: {
            enable: true,
            mouse: {
                distance: 300
            },
            detect_on: 'canvas', // "canvas" or "window"
            mode: 'grab',
            line_linked: {
                opacity: .5
            },
            events: {
                onclick: {
                    enable: true,
                    mode: 'push', // "push" or "remove"
                    nb: 4
                }
            }
        },
        /!* Retina Display Support *!/
        retina_detect: true
    });*/
})