var up_for_what = '';
$(function(){
    $("#up-img-touch").click(function(){
        up_for_what = 'img';
        initUpLoad();
    });

    $("#up-qr-touch").click(function(){
        up_for_what = 'qr';
        initUpLoad();
    });

    $("#up-logo-touch").click(function(){
        up_for_what = 'logo';
        initUpLoad();
    });

    $("#up-avatar-touch").click(function(){
        up_for_what = 'avatar';
        initUpLoad();
    });
});

var initUpLoad = function()
{
    $("#up-modal-frame").modal({});
};

$(function() {
    'use strict';
    // 初始化
    var cropRatio = '1';

    // 只要不是用户设置、关于我们调用了，就以4/3的比例裁剪
    if(location.href.indexOf("user") == -1 && location.href.indexOf("info") == -1)
    {
        cropRatio = '1.31';
    }
    var $image = $('#up-img-show');
    $image.cropper({
        aspectRatio: cropRatio,
        autoCropArea:0.8,
        preview: '.up-pre-after',
        responsive:true
    });

    // 上传图片
    var $inputImage = $('.up-modal-frame .up-img-file');
    var URL = window.URL || window.webkitURL;
    var blobURL;

    if (URL) {
        $inputImage.change(function () {

            var files = this.files;
            var file;

            if (files && files.length) {
                file = files[0];

                if (/^image\/\w+$/.test(file.type)) {
                    blobURL = URL.createObjectURL(file);
                    $image.one('built.cropper', function () {
                        // Revoke when load complete
                        URL.revokeObjectURL(blobURL);
                    }).cropper('reset').cropper('replace', blobURL);
                    $inputImage.val('');
                } else {
                    window.alert('Please choose an image file.');
                }
            }
        });
    } else {
        $inputImage.prop('disabled', true).parent().addClass('disabled');
    }

    //绑定上传事件
    $('.up-modal-frame .up-btn-ok').on('click',function(){
        var img_src=$image.attr("src");
        if(img_src=="")
        {
            layer.msg("没有选择上传的图片");
            return false;
        }

        layer.load(1, {
            shade: [0.1,'#FFF'] //0.1透明度的白色背景
        });

        var url="/admin/fileUpload/imageUploadBase64";
        var width=200;
        var height=200;

        //控制裁剪图片的大小
        var canvas=$image.cropper('getCroppedCanvas',{width: width,height: height});
        var data=canvas.toDataURL(); //转成base64
        $.ajax( {
            url:url,
            dataType:'json',
            type: "POST",
            data: {"image":data.toString()},
            success: function(data, textStatus){
                layer.closeAll('loading');
                if(data.error==0){
                    var imguri = data.path+data.url;
                    $("#up-"+up_for_what+"-touch").attr("src",imguri);
                    $("#"+up_for_what+"-save-path").val(imguri);
                    $("#up-modal-frame").modal('close');
                }
                else
                {

                }
            },
            error: function(){
                layer.closeAll('loading');
                layer.msg("图片上传失败");
            }
        });

    });

    $('#up-btn-left').on('click',function(){
        $("#up-img-show").cropper('rotate', 90);
    });
    $('#up-btn-right').on('click',function(){
        $("#up-img-show").cropper('rotate', -90);
    });
});