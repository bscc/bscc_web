package com.bscc.web;

import com.bscc.admin.dict.DictService;
import com.bscc.admin.industry.IndustryService;
import com.bscc.admin.info.InfoService;
import com.bscc.admin.news.NewsService;
import com.bscc.admin.news_template.NewsTemplateService;
import com.bscc.admin.product_case.ProductCaseService;
import com.bscc.admin.solution.SolutionService;
import com.bscc.admin.user.UserService;
import com.bscc.common.interceptor.GetActionKey;
import com.bscc.common.model.*;
import com.jfinal.aop.Before;
import com.jfinal.aop.Clear;
import com.jfinal.core.Controller;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Clear
@Before(GetActionKey.class)
public class WebIndexController extends Controller
{
    private static NewsService newsService = new NewsService();
    private static NewsTemplateService newsTemplateService = new NewsTemplateService();
    private static DictService dictService = new DictService();
    private static IndustryService industryService = new IndustryService();
    private static ProductCaseService productCaseService = new ProductCaseService();
    private static SolutionService solutionService = new SolutionService();
    private static UserService userService = new UserService();

    public void index()
    {
        setAttr("productCases", productCaseService.paginate(1, 3).getList());
        List<Map> newsChannels = new ArrayList<Map>();
        List<Dict> channels = dictService.findDictByMark("news_type");

        if(null != channels && channels.size() > 0)
        {
            for(Dict d:channels)
            {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("type", d.getValue());
                map.put("typeName", d.getName());
                map.put("data", newsService.pageForWeb(1, 5, Integer.valueOf(d.getValue()), 0).getList());
                newsChannels.add(map);
            }
        }
        setAttr("newsGroup",newsChannels);
        render("/index.html");
    }

    public void method()
    {
        setAttr("slutionPage", solutionService.findList(0));
        render("/method_list.html");
    }

    public void methodDetail()
    {
        setAttr("solution", solutionService.findById(getParaToInt()));
        render("/method_detail.html");
    }

    public void news()
    {
        List<Dict> channels = dictService.findDictByMark("news_type");
        String channelStr = getPara("channel");
        int currentChannel = getCurrent(channelStr, channels);
        setAttr("channels", channels);
        setAttr("currentChannel", currentChannel);
        setAttr("newsPage", newsService.pageForWeb(getParaToInt(0, 1), 10, currentChannel, 0));
        render("/newlist.html");
    }

    public void newsDetail()
    {
        News news = newsService.findById(getParaToInt());
        if(null != news)
        {
            setAttr("news", news);
            List<Dict> channels = dictService.findDictByMark("news_type");
            if(null != channels && channels.size() > 0)
            {
                for(Dict c:channels)
                {
                    if(news.getChannel().equals(Integer.valueOf(c.getValue())))
                    {
                        setAttr("channelName", c.getName());
                        break;
                    }
                }
            }
            setAttr("sameChannel", newsService.pageForWeb(1, 4, news.getChannel(), news.getId()).getList());
            if(news.getType() == 1)
            {
                setAttr("productsCase", productCaseService.paginate(1, 8).getList());
                render("/newcenter.html");
            }
            else if(news.getType() == 2)
            {
                NewsTemplate newsTemplate = newsTemplateService.findById(news.getTemplateId());
                if(null != newsTemplate && StringUtils.isNotBlank(newsTemplate.getUri()))
                {
                    render(newsTemplate.getUri());
                }
                else
                {
                    render("/newcenter.html");
                }
            }
            else
            {
                redirect(news.getLink());
            }
        }
    }

    public void caze()
    {
        List<Dict> caseTypes = dictService.findDictByMark("case_type");
        String type = getPara("type");
        int currentType = getCurrent(type, caseTypes);
        setAttr("types", caseTypes);
        setAttr("currentType", currentType);
        setAttr("casePage", productCaseService.findList(currentType));
        render("/case_list.html");
    }

    public void cazeDetail()
    {
        ProductCase productCase = productCaseService.findById(getParaToInt());
        setAttr("productCase", productCase);
        List<Dict> caseTypes = dictService.findDictByMark("case_type");
        if(null != productCase && null != productCase.getType())
        {
            for(Dict d: caseTypes)
            {
                if(productCase.getType().equals(Integer.valueOf(d.getValue())))
                {
                    setAttr("typeName", d.getName());
                    break;
                }
            }
        }
        render("/case_detail.html");
    }

    public void about()
    {
        setAttr("menbers", userService.findListForWeb());
        render("/about_us.html");
    }

    public void productServer()
    {
        render("/productserver.html");
    }

    private int getCurrent(String str, List<Dict> dicts)
    {
        int cur;
        if(StringUtils.isBlank(str))
        {
            if(null != dicts && dicts.size() > 0)
            {
                cur = Integer.valueOf(dicts.get(0).getValue());
            }
            else
            {
                cur = -1;
            }
        }
        else
        {
            cur = Integer.valueOf(str);
        }
        return cur;
    }
}
