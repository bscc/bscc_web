package com.bscc.admin.info;

import com.bscc.admin.user.UserService;
import com.bscc.common.model.Info;
import com.jfinal.core.Controller;

public class InfoController extends Controller
{
    static InfoService service = new InfoService();
    static UserService userService = new UserService();

    public void index()
    {
        setAttr("userList", userService.findList());
        setAttr("info", service.findInfo());
        render("info.html");
    }

    public void save()
    {
        service.save(getModel(Info.class));
        redirect("/admin/info");
    }
}
