package com.bscc.admin.info;

import com.bscc.common.model.Info;

import java.util.List;

public class InfoService
{
    private static final Info dao = new Info().dao();

    public Info findInfo()
    {
        Info info = new Info();
        List<Info> infoList = dao.find("SELECT * FROM bw_info ORDER BY id");
        if(null != infoList && infoList.size() > 0)
            info = infoList.get(0);
        return info;
    }

    public void save(Info info)
    {
        if(null != info.getId() && null != dao.findById(info.getId()))
            info.update();
        else
            info.save();
    }
}
