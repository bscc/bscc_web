package com.bscc.admin.product_case;

import com.bscc.admin.dict.DictService;
import com.bscc.admin.industry.IndustryService;
import com.bscc.common.model.ProductCase;
import com.jfinal.core.Controller;

public class ProductCaseController extends Controller
{
    private static ProductCaseService service = new ProductCaseService();
    private static IndustryService industryService = new IndustryService();
    private static DictService dictService = new DictService();

    public void index()
    {
        setAttr("types", dictService.findDictByMark("case_type"));
        setAttr("industryList", industryService.findList());
        setAttr("productCasePage", service.paginate(getParaToInt(0, 1), 10));
        render("productCase.html");
    }


    public void add()
    {
        setAttr("types", dictService.findDictByMark("case_type"));
        setAttr("industryList", industryService.findList());
        setAttr("productCase", new ProductCase());
    }

    public void save()
    {
        service.save(getModel(ProductCase.class));
        redirect("/admin/product_case");
    }

    public void edit()
    {
        setAttr("types", dictService.findDictByMark("case_type"));
        setAttr("industryList", industryService.findList());
        setAttr("productCase", service.findById(getParaToInt()));
    }

    public void update()
    {
        service.update(getModel(ProductCase.class));
        redirect("/admin/product_case");
    }

    public void delete()
    {
        service.deleteById(getParaToInt());
        redirect("/admin/product_case");
    }
}
