package com.bscc.admin.product_case;

import com.bscc.common.model.ProductCase;
import com.jfinal.plugin.activerecord.Page;

import java.util.List;

public class ProductCaseService
{
    private final static ProductCase dao = new ProductCase();

    public Page<ProductCase> paginate(int pageNumber, int pageSize)
    {
        return dao.paginate(pageNumber, pageSize, "SELECT *","FROM bw_product_case ORDER BY time DESC");
    }

    public List<ProductCase> findList(int currentType)
    {
        return dao.find("SELECT * FROM bw_product_case"+(currentType!=0?(" WHERE type="+currentType):"")+" ORDER BY time DESC");
    }

    public ProductCase findById(int id)
    {
        return dao.findById(id);
    }

    public void deleteById(int id)
    {
        dao.deleteById(id);
    }

    public void save(ProductCase productCase)
    {
        productCase.save();
    }

    public void update(ProductCase productCase)
    {
        productCase.update();
    }
}
