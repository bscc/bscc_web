package com.bscc.admin.dict;

import com.bscc.common.model.Dict;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import org.apache.commons.lang3.StringUtils;

public class DictController extends Controller
{
    static DictService service = new DictService();

    public void index()
    {
        setAttr("dictType", service.findTypeList());
        setAttr("dictPage", service.findTreeList());
        render("dict.html");
    }

    public void add()
    {
        String pid = getPara("pid");
        Dict dict = new Dict();
        if(StringUtils.isNotBlank(pid))
        {
            String pname = getPara("pname");
            dict.setPid(Integer.valueOf(pid));
            dict.setPname(pname);
            dict.setType(2);
        }
        else
        {
            dict.setPid(0);
            dict.setType(1);
        }

        setAttr("dict", dict);
    }

    @Before(DictValidator.class)
    public void save()
    {
        service.save(getModel(Dict.class));
        redirect("/admin/dict");
    }


    public void edit()
    {
        setAttr("dict", service.findById(getParaToInt()));
    }


    @Before(DictValidator.class)
    public void update()
    {
        service.update(getModel(Dict.class));
        redirect("/admin/dict");
    }

    public void delete()
    {
        service.deleteById(getParaToInt());
        redirect("/admin/dict");
    }
}
