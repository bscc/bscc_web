package com.bscc.admin.dict;

import com.bscc.common.model.Menu;
import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;

public class DictValidator  extends Validator
{
    protected void validate(Controller controller)
    {
        validateRequiredString("dict.name", "nameMsg", "请输入字典名!");
    }

    protected void handleError(Controller controller)
    {
        controller.keepModel(Menu.class);

        String actionKey = getActionKey();
        if (actionKey.equals("/admin/dict/save"))
            controller.render("add.html");
        else if (actionKey.equals("/admin/dict/update"))
            controller.render("edit.html");
    }
}
