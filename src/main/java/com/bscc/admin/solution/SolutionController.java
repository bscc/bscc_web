package com.bscc.admin.solution;

import com.bscc.admin.industry.IndustryService;
import com.bscc.common.model.Solution;
import com.jfinal.core.Controller;

public class SolutionController extends Controller
{
    static SolutionService service = new SolutionService();
    static IndustryService industryService = new IndustryService();

    public void index()
    {
        setAttr("industryList", industryService.findList());
        setAttr("solutionPage", service.paginate(getParaToInt(0, 1), 10));
        render("solution.html");
    }


    public void add()
    {
        setAttr("industryList", industryService.findList());
        setAttr("solution", new Solution());
    }

    public void save()
    {
        service.save(getModel(Solution.class));
        redirect("/admin/solution");
    }

    public void edit()
    {
        setAttr("industryList", industryService.findList());
        setAttr("solution", service.findById(getParaToInt()));
    }

    public void update()
    {
        service.update(getModel(Solution.class));
        redirect("/admin/solution");
    }

    public void delete()
    {
        service.deleteById(getParaToInt());
        redirect("/admin/solution");
    }
}
