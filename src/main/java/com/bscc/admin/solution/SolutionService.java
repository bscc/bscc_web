package com.bscc.admin.solution;

import com.bscc.common.model.Solution;
import com.jfinal.plugin.activerecord.Page;

import java.util.List;

public class SolutionService
{
    private final static Solution dao = new Solution().dao();

    public Page<Solution> paginate(int pageNumber, int pageSize)
    {
        return dao.paginate(pageNumber, pageSize, "SELECT *","FROM bw_solution ORDER BY time DESC");
    }

    public List<Solution> findList(int industry)
    {
        return dao.find("SELECT * FROM bw_solution"+(industry!=0?(" WHERE industry="+industry):"")+" ORDER BY time DESC");
    }

    public Solution findById(int id)
    {
        return dao.findById(id);
    }

    public void deleteById(int id)
    {
        dao.deleteById(id);
    }

    public void save(Solution solution)
    {
        solution.save();
    }

    public void update(Solution solution)
    {
        solution.update();
    }
}
