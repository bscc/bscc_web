package com.bscc.admin.industry;

import com.bscc.common.model.Industry;
import com.jfinal.core.Controller;
import org.apache.commons.lang3.StringUtils;

public class IndustryController extends Controller
{
    static IndustryService service = new IndustryService();


    public void index()
    {
        setAttr("industryPage", service.findList());
        render("industry.html");
    }

    public void add()
    {
        String pid = getPara("pid");
        Industry industry = new Industry();
        if(StringUtils.isNotBlank(pid))
        {
            String pname = getPara("pname");
            String level = getPara("level");
            industry.setPid(Integer.valueOf(pid));
            industry.setPname(pname);
            industry.setLevel(Integer.valueOf(level)+1);
        }
        else
        {
            industry.setPid(0);
            industry.setLevel(1);
        }
        setAttr("industry", industry);
    }

    public void save()
    {
        service.save(getModel(Industry.class));
        redirect("/admin/industry");
    }


    public void edit()
    {
        setAttr("industry", service.findById(getParaToInt()));
    }


    public void update()
    {
        service.update(getModel(Industry.class));
        redirect("/admin/industry");
    }

    public void delete()
    {
        service.deleteById(getParaToInt());
        redirect("/admin/industry");
    }

}
