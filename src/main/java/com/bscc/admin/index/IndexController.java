package com.bscc.admin.index;

import com.bscc.common.model.User;
import com.bscc.admin.user.UserService;
import com.bscc.admin.user.UserValidator;
import com.jfinal.aop.Before;
import com.jfinal.aop.Clear;
import com.jfinal.core.Controller;

/**
 * IndexController
 */
public class IndexController extends Controller
{
	static UserService userService = new UserService();

	@Clear
	public void index()
	{
		User user = getSessionAttr("user");
		if(null != user)
			redirect("/admin/main");
		else
			redirect("/admin/login");

	}

	public void main()
	{

	}

	@Clear
	public void login()
	{

	}

	@Clear
	public void reLogin()
	{
		setAttr("msg", "请先登陆");
		render("login.html");
	}

	@Clear
	@Before(UserValidator.class)
	public void signIn()
	{
		User user = getModel(User.class);
		user = userService.verifySignIn(user);
		if(null != user)
		{
			setSessionAttr("user", user);
			redirect("/admin");
		}
		else
			redirect("/admin/login");
	}

	@Clear
	public void signOut()
	{
		removeSessionAttr("user");
		redirect("/admin/login");
	}
}



