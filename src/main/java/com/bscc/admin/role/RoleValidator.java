package com.bscc.admin.role;

import com.bscc.common.model.Role;
import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;

public class RoleValidator extends Validator
{
    protected void validate(Controller controller)
    {
        validateRequiredString("role.name", "nameMsg", "请输入角色名!");
    }

    protected void handleError(Controller controller)
    {
        controller.keepModel(Role.class);

        String actionKey = getActionKey();
        if (actionKey.equals("/admin/role/save"))
            controller.render("add.html");
        else if (actionKey.equals("/admin/role/update"))
            controller.render("edit.html");
    }
}
