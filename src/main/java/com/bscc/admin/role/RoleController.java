package com.bscc.admin.role;

import com.bscc.common.model.Role;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;

public class RoleController extends Controller
{

    static RoleService service = new RoleService();

    public void index()
    {
        setAttr("rolePage", service.paginate(getParaToInt(0, 1), 10));
        render("role.html");
    }

    public void add()
    {

    }

    @Before(RoleValidator.class)
    public void save()
    {
        service.save(getModel(Role.class));
        redirect("/admin/role");
    }

    public void edit()
    {
        setAttr("role", service.findById(getParaToInt()));
    }

    @Before(RoleValidator.class)
    public void update()
    {
        service.update(getModel(Role.class));
        redirect("/admin/role");
    }

    public void delete() {
        service.deleteById(getParaToInt());
        redirect("/admin/role");
    }
}
