package com.bscc.admin.news_template;

import com.bscc.common.model.NewsTemplate;
import com.jfinal.plugin.activerecord.Page;

import java.util.List;

public class NewsTemplateService
{
    private final static NewsTemplate dao = new NewsTemplate().dao();


    public Page<NewsTemplate> paginate(int pageNumber, int pageSize)
    {
        return dao.paginate(pageNumber, pageSize, "SELECT *","FROM bw_news_template ORDER BY id ASC");
    }

    public List<NewsTemplate> findList()
    {
        return dao.find("SELECT * FROM bw_news_template ORDER BY id ASC");
    }

    public NewsTemplate findById(int id)
    {
       return dao.findById(id);
    }

    public void deleteById(int id)
    {
        dao.deleteById(id);
    }

    public void save(NewsTemplate newsTemplate)
    {
        newsTemplate.save();
    }

    public void update(NewsTemplate newsTemplate)
    {
        newsTemplate.update();
    }
}
