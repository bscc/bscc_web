package com.bscc.admin.news_template;

import com.bscc.common.model.NewsTemplate;
import com.jfinal.core.Controller;

public class NewsTemplateController extends Controller
{
    static NewsTemplateService service = new NewsTemplateService();

    public void index()
    {
        setAttr("newsTemplatePage", service.paginate(getParaToInt(0, 1), 10));
        render("newsTemplate.html");
    }


    public void add()
    {

    }

    public void save()
    {
        service.save(getModel(NewsTemplate.class));
        redirect("/admin/news_template");
    }

    public void edit()
    {
        setAttr("newsTemplate", service.findById(getParaToInt()));
    }

    public void update()
    {
        service.update(getModel(NewsTemplate.class));
        redirect("/admin/news_template");
    }

    public void delete()
    {
        service.deleteById(getParaToInt());
        redirect("/admin/news_template");
    }
}
