package com.bscc.admin.news;

import com.bscc.common.model.News;
import com.jfinal.plugin.activerecord.Page;

import java.util.List;

public class NewsService
{
    private static final News dao = new News().dao();

    public Page<News> paginate(int pageNumber, int pageSize)
    {
        return dao.paginate(pageNumber, pageSize, "SELECT *","FROM bw_news ORDER BY time ASC");
    }

    public News findById(int id)
    {
        return dao.findById(id);
    }

    public void deleteById(int id)
    {
        dao.deleteById(id);
    }

    public void save(News news)
    {
        filter(news);
        news.save();
    }

    public void update(News news)
    {
        filter(news);
        news.update();
    }


    private void filter(News news)
    {
        switch (news.getType())
        {
            case 1:
                news.setTemplateId(null);
                news.setLink("");
                break;
            case 2:
                news.setLink("");
                break;
            case 3:
                news.setTemplateId(null);
                news.setContent("");
                break;
        }
    }


    public Page<News> pageForWeb(int pageNumber, int pageSize, int channel, int withOut)
    {
        return dao.paginate(pageNumber, pageSize, "SELECT *","FROM bw_news WHERE status=2 AND channel="+channel+(withOut>0?(" AND id!="+withOut):"")+" ORDER BY top DESC,time ASC");
    }
}
