package com.bscc.admin.news;

import com.bscc.common.model.News;
import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;

public class NewsValidator extends Validator
{
    protected void validate(Controller controller)
    {
        validateRequiredString("news.title", "titleMsg", "请输入标题!");
    }

    protected void handleError(Controller controller)
    {
        controller.keepModel(News.class);

        String actionKey = getActionKey();
        if (actionKey.equals("/admin/news/save"))
            controller.render("add.html");
        else if (actionKey.equals("/admin/news/update"))
            controller.render("edit.html");
    }
}
