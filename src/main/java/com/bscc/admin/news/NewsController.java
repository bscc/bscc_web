package com.bscc.admin.news;

import com.bscc.admin.dict.DictService;
import com.bscc.admin.news_template.NewsTemplateService;
import com.bscc.admin.user.UserService;
import com.bscc.common.model.News;
import com.bscc.common.model.User;
import com.jfinal.core.Controller;

public class NewsController extends Controller
{
    private static NewsService service = new NewsService();
    private static UserService userService = new UserService();
    private static DictService dictService = new DictService();
    private static NewsTemplateService newsTemplateService = new NewsTemplateService();

    public void index()
    {
        setAttr("channels", dictService.findDictByMark("news_type"));
        setAttr("userList", userService.findList());
        setAttr("newsPage", service.paginate(getParaToInt(0, 1), 10));
        render("news.html");
    }

    public void add()
    {
        setAttr("channels", dictService.findDictByMark("news_type"));
        setAttr("templateList", newsTemplateService.findList());
        setAttr("news", new News());
    }


    public void save()
    {
        News news = getModel(News.class);
        User user = getSessionAttr("user");
            if (null != user)
        {
           news.setCreator(user.getId());
        }
        service.save(news);
        redirect("/admin/news");
    }

    public void edit()
    {
        setAttr("channels", dictService.findDictByMark("news_type"));
        setAttr("templateList", newsTemplateService.findList());
        setAttr("news", service.findById(getParaToInt()));
    }

    public void update()
    {
        service.update(getModel(News.class));
        redirect("/admin/news");
    }

    public void delete()
    {
        service.deleteById(getParaToInt());
        redirect("/admin/news");
    }
}
