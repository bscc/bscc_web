package com.bscc.admin.menu;

import com.bscc.common.model.Menu;
import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;

public class MenuValidator extends Validator
{
    protected void validate(Controller controller)
    {
        validateRequiredString("menu.name", "nameMsg", "请输入菜单名!");
    }


    protected void handleError(Controller controller)
    {
        controller.keepModel(Menu.class);

        String actionKey = getActionKey();
        if (actionKey.equals("/admin/menu/save"))
            controller.render("add.html");
        else if (actionKey.equals("/admin/menu/update"))
            controller.render("edit.html");
    }
}
