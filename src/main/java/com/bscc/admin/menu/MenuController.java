package com.bscc.admin.menu;

import com.bscc.common.model.Menu;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import org.apache.commons.lang3.StringUtils;

public class MenuController extends Controller
{
    static MenuService service = new MenuService();

    public void index()
    {
        setAttr("menuPage", service.findList());
        render("menu.html");
    }

    public void add()
    {
        String pid = getPara("pid");
        int ptype = 0;
        Menu menu = new Menu();
        if(StringUtils.isNotBlank(pid))
        {
            String pname = getPara("pname");
            ptype = getParaToInt("ptype");
            menu.setPname(pname);
            menu.setPid(Integer.valueOf(pid));
        }
        else
        {
            menu.setPid(0);
        }
        setAttr("ptype", ptype);
        setAttr("menu",menu);
    }

    @Before(MenuValidator.class)
    public void save()
    {
        service.save(getModel(Menu.class));
        redirect("/admin/menu");
    }


    public void edit()
    {
        setAttr("menu", service.findById(getParaToInt()));
    }

    @Before(MenuValidator.class)
    public void update()
    {
        service.update(getModel(Menu.class));
        redirect("/admin/menu");
    }

    public void delete()
    {
        service.deleteById(getParaToInt());
        redirect("/admin/menu");
    }
}
