package com.bscc.admin.user;

import com.bscc.common.model.User;
import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;

public class UserValidator extends Validator
{
    protected void validate(Controller controller)
    {
        validateRequiredString("user.account", "accountMsg", "请输入账号!");
    }

    protected void handleError(Controller controller)
    {
        controller.keepModel(User.class);

        String actionKey = getActionKey();
        if (actionKey.equals("/admin/user/save"))
            controller.redirect("/admin/user/add");
        else if (actionKey.equals("/admin/user/update"))
            controller.render("/admin/user/edit");
    }
}
