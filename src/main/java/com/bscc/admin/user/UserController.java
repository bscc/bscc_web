package com.bscc.admin.user;

import com.bscc.admin.role.RoleService;
import com.bscc.common.model.User;
import com.bscc.common.utils.MD5Utils;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import org.apache.commons.lang3.StringUtils;

public class UserController extends Controller
{
    static UserService service = new UserService();
    static RoleService roleService = new RoleService();

    public void index()
    {
        setAttr("roleList", roleService.roleList(false));
        setAttr("userPage", service.paginate(getParaToInt(0, 1), 10));
        render("user.html");
    }

    public void add()
    {
        setAttr("roleList", roleService.roleList(true));
        setAttr("user", new User());
    }


    @Before(UserValidator.class)
    public void save()
    {
        service.save(getModel(User.class));
        redirect("/admin/user");
    }

    public void edit()
    {
        setAttr("roleList", roleService.roleList(true));
        setAttr("user", service.findById(getParaToInt()));
    }

    public void update()
    {
        service.update(getModel(User.class));
        redirect("/admin/user");
    }

    public void delete()
    {
        service.deleteById(getParaToInt());
        redirect("/admin/user");
    }


    public void userSet()
    {
        User user = getSessionAttr("user");
        setAttr("user", user);
        render("user_set.html");
    }

    public void updateByUser()
    {
        User user = getSessionAttr("user");
        User upUser = getModel(User.class);
        String oldPassword = getPara("oldPassword");
        String newPassword = getPara("newPassword");
        String repeatNewPassword = getPara("repeatNewPassword");
        int success = 0;
        if(StringUtils.isNotBlank(newPassword) || StringUtils.isNotBlank(repeatNewPassword))
        {
            if(StringUtils.isBlank(oldPassword))
            {
                setAttr("oldErrorMsg", "请输入旧密码");
                success = 1;
            }
            else if(MD5Utils.getMD5Twice(oldPassword).equals(user.getPassword()))
            {
                setAttr("oldErrorMsg", "旧密码错误");
            }

            if(StringUtils.isNotBlank(newPassword) && StringUtils.isNotBlank(repeatNewPassword))
            {
                if(!newPassword.equals(repeatNewPassword))
                {
                    setAttr("repErrorMsg", "两次输入密码不匹配");
                    success = 1;
                }
            }
            else if(StringUtils.isBlank(repeatNewPassword))
            {
                setAttr("repErrorMsg", "请再次输入新密码");
                success = 1;
            }
            else
            {
                setAttr("newErrorMsg", "请输入新密码");
                success = 1;
            }
        }
        if(success == 0)
        {
            if(null != upUser.getName())
            {
                user.setName(upUser.getName());
            }
            if(null != upUser.getAvatar())
            {
                user.setAvatar(upUser.getAvatar());
            }
            if(StringUtils.isNotBlank(newPassword))
            {
                user.setPassword(newPassword);
            }
            setSessionAttr("user", user);
            service.update(user);
        }

        setAttr("success", success);
        setAttr("user", user);
        redirect("/admin/user/userSet");
    }
}
