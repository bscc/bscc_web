package com.bscc.common.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class CommonUtils
{
    public static String getCurrentDate()
    {
        return new SimpleDateFormat("yyyyMMdd").format(new Date());
    }

    public static String getCurrentTime()
    {
        return new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
    }

    public static String getUUID()
    {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }
}
