package com.bscc.common;

import com.bscc.admin.file.FileUploadController;
import com.bscc.admin.industry.IndustryController;
import com.bscc.admin.info.InfoController;
import com.bscc.admin.news.NewsController;
import com.bscc.admin.news_template.NewsTemplateController;
import com.bscc.admin.product_case.ProductCaseController;
import com.bscc.admin.solution.SolutionController;
import com.bscc.common.directive.IfHasAuth;
import com.bscc.common.interceptor.GetActionKey;
import com.bscc.common.model._MappingKit;
import com.bscc.admin.dict.DictController;
import com.bscc.admin.index.IndexController;
import com.bscc.admin.menu.MenuController;
import com.bscc.admin.role.RoleController;
import com.bscc.admin.user.UserController;
import com.bscc.common.interceptor.UserInterceptor;
import com.bscc.web.WebIndexController;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.ext.interceptor.SessionInViewInterceptor;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.template.Engine;

/**
 * API引导式配置
 */
public class BWSConfig extends JFinalConfig
{
	
	/**
	 * 运行此 main 方法可以启动项目，此main方法可以放置在任意的Class类定义中，不一定要放于此
	 * 
	 * 使用本方法启动过第一次以后，会在开发工具的 debug、run config 中自动生成
	 * 一条启动配置，可对该自动生成的配置再添加额外的配置项，例如 VM argument 可配置为：
	 * -XX:PermSize=64M -XX:MaxPermSize=256M
	 */
	public static void main(String[] args)
	{
		JFinal.start("src/main/webapp", 9090, "/");
	}
	
	/**
	 * 配置常量
	 */
	public void configConstant(Constants me)
	{
		// 加载少量必要配置，随后可用PropKit.get(...)获取值
		PropKit.use("config.properties");
		me.setDevMode(PropKit.getBoolean("devMode", false));
		me.setBaseUploadPath(PropKit.get("fileUploadPath"));
		me.setError404View("/admin/error/404.html");
		me.setError500View("/admin/error/500.html");
	}
	
	/**
	 * 配置路由
	 */
	public void configRoute(Routes me)
	{
		me.add("/", WebIndexController.class);
		me.add("/admin", IndexController.class, "/admin/index");	// 第三个参数为该Controller的视图存放路径
		me.add("/admin/user", UserController.class);			// 第三个参数省略时默认与第一个参数值相同，在此即为 "/user"
		me.add("/admin/menu", MenuController.class);
		me.add("/admin/role", RoleController.class);
		me.add("/admin/dict", DictController.class);
		me.add("/admin/news", NewsController.class);
		me.add("/admin/fileUpload", FileUploadController.class);
		me.add("/admin/info", InfoController.class);
		me.add("/admin/news_template", NewsTemplateController.class);
		me.add("/admin/industry", IndustryController.class);
		me.add("/admin/solution", SolutionController.class);
		me.add("/admin/product_case", ProductCaseController.class);
	}
	
	public void configEngine(Engine me)
	{
		me.addDirective("ifHasAuth", new IfHasAuth());
		me.addSharedFunction("/admin/common/_layout.html");
		me.addSharedFunction("/admin/common/_paginate.html");
		me.addSharedFunction("/public/_web_layout.html");
		me.addSharedFunction("/public/_web_paginate.html");
	}
	
	/**
	 * 配置插件
	 */
	public void configPlugin(Plugins me)
	{
		// 配置 druid 数据库连接池插件
		DruidPlugin druidPlugin = new DruidPlugin(PropKit.get("jdbcUrl"), PropKit.get("user"), PropKit.get("password").trim());
		me.add(druidPlugin);
		
		// 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
		// 所有映射在 MappingKit 中自动化搞定
		_MappingKit.mapping(arp);
		me.add(arp);
	}
	
	public static DruidPlugin createDruidPlugin()
	{
		return new DruidPlugin(PropKit.get("jdbcUrl"), PropKit.get("user"), PropKit.get("password").trim());
	}
	
	/**
	 * 配置全局拦截器
	 */
	public void configInterceptor(Interceptors me)
	{
		me.addGlobalActionInterceptor(new SessionInViewInterceptor());
		me.addGlobalActionInterceptor(new UserInterceptor());
		me.addGlobalActionInterceptor(new GetActionKey());
	}
	
	/**
	 * 配置处理器
	 */
	public void configHandler(Handlers me)
	{
		me.add(new ContextPathHandler("ctx_path"));
	}
}
