package com.bscc.common.model;

import com.bscc.common.model.base.BaseMenu;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class Menu extends BaseMenu<Menu>
{
	public static final Menu dao = new Menu().dao();

	private int childCount;
	private String pname;

	public int getChildCount() {
		return childCount;
	}

	public void setChildCount(int childCount) {
		this.childCount = childCount;
	}


	public String getPname() {
		return pname;
	}

	public void setPname(String pname) {
		this.pname = pname;
	}
}
