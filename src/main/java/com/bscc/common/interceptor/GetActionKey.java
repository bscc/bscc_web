package com.bscc.common.interceptor;

import com.bscc.admin.info.InfoService;
import com.bscc.common.model.Info;
import com.jfinal.aop.Invocation;
import com.jfinal.aop.Interceptor;

public class GetActionKey implements Interceptor
{
    private static InfoService infoService = new InfoService();

    public void intercept(Invocation inv)
    {
        System.out.println(inv.getActionKey());
        inv.getController().setAttr("actionKey", inv.getActionKey());
        inv.getController().setAttr("corpInfo", infoService.findInfo());

        inv.invoke();
    }
}
